{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "prateem";
  home.homeDirectory = "/home/prateem";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "22.11"; # Please read the comment before changing.

  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/emacs-overlay/archive/master.tar.gz;
    }))
  ];

  # How to install and enable fonts using home-manager?
  # https://discourse.nixos.org/t/home-manager-nerdfonts/11226/2?u=prateem
  fonts.fontconfig.enable = true;
  
  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [
    # # Adds the 'hello' command to your environment. It prints a friendly
    # # "Hello, world!" when run.
    # pkgs.hello

    # It is sometimes useful to fine-tune packages, for example, by applying
    # overrides. You can do that directly here, just don't forget the
    # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" ]; })
    nerd-fonts.fira-code

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    # dev tools
    git
    curl
    wget
    jq
    tree
    vim
    gnupg
    asciinema
    asciinema-agg
    htop
    tmux
    screen
    nushell
    byobu
    guile_3_0
    nix-tree
    dfu-util
    # postman
    # docker

    # c & c++
    # gcc
    gnumake
    cmake
    clang
    clang-tools # provides clangd
    
    # # java
    jdk
    # maven
    # jdt-language-server

    # # google cloud
    # google-cloud-sdk

    # scala
    coursier
    sbt
    metals

    # # haskell
    # cabal-install
    # ghc
    
    # # fonts
    # fira-code
    cascadia-code
  ];

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  # Source: https://discourse.nixos.org/t/running-shell-scripts-in-home-manager-standalone-nix-flakes/29538?u=prateem
  home.file = {
    # # Building this configuration will create a copy of 'dotfiles/screenrc' in
    # # the Nix store. Activating the configuration will then make '~/.screenrc' a
    # # symlink to the Nix store copy.
    ".screenrc".source = dotfiles/screenrc;

    # # You can also set the file content immediately.
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
  };

  programs.git = {
    enable = true;
    userName = "Prateem Mandal";
    userEmail = "prateem@gmail.com";
  };

  programs.nushell = {
    enable = true;
    extraConfig = ''
      $env.config.show_banner = false
      $env.config.buffer_editor = "emacs"
      $env.TRANSIENT_PROMPT_COMMAND = ""
    '';
  };
  
  programs.emacs = {
    enable = true;
    package = pkgs.emacs-git;
    # package = pkgs.emacs;
    extraPackages = epkgs: with epkgs; [
      use-package
      cl-lib
      nix-mode
      cmake-mode
      dockerfile-mode
      magit
      exec-path-from-shell
      smex
      projectile
      helm
      helm-company
      helm-descbinds
      helm-swoop
      marginalia
      which-key
      which-key-posframe
      paredit
      # geiser
      # geiser-guile
      flycheck
      # flycheck-guile
      yasnippet
      yasnippet-snippets
      company
      company-org-block
      company-shell
      treemacs
      tramp
      tramp-auto-auth
      tramp-nspawn
      tramp-term
      tramp-hdfs
      tramp-theme
      keycast
      request
      esxml
      vterm
      yaml-mode
      pdf-tools
      unicode-fonts
      # dante
      lsp-mode
      lsp-ui
      # helm-lsp
      # lsp-treemacs
      haskell-mode
      flycheck-haskell
      lsp-haskell
      company-cabal

      # scala
      # lsp-metals
      posframe
      # dap-mode
      scala-mode
      sbt-mode
      
      # # Fira Code
      # fira-code-mode
      
      # Java related
      exec-path-from-shell
      projectile
      which-key
      # lsp-java

      # Python related
      yapfify
      lsp-pyright
      python
      # dap-mode

      # Rust related
      rust-mode
      
      # nXML
      # hideshow
      # sgml-mode
      # nxml-mode
      
      # color themes
      color-theme-sanityinc-solarized
      color-theme-sanityinc-tomorrow
    ];
    extraConfig = ''
      (require 'cl-lib)
      (require 'package)

      (setq standard-indent 2)

      ;; default is 800kb, measured in bytes
      (setf gc-cons-threshold (* 50 1000 1000))

      ;; profile emacs startup
      (add-hook 'emacs-startup-hook
	              (lambda ()
	                (message "Emacs loaded in %s seconds with %d garbage collections."
		              (emacs-init-time "%.2f")
		              gcs-done)))

      ;; silence compiler warnings
      (setf native-comp-async-report-warnings-errors nil)

      ;; performance tuning for lsp-haskell
      (setf read-process-output-max (* 1024 1024))

      ;; set the right directory to store the native comp cache
      (add-to-list 'native-comp-eln-load-path (expand-file-name "eln-cache/" user-emacs-directory))

      ;; set the compilation level to 3
      (setf native-comp-speed 3)

      ;; set compiler options
      (setf native-comp-compiler-options '("-O3"))

      ;; load shell environment variables into emacs
      (exec-path-from-shell-initialize)

      ;; language environment
      (set-language-environment "UTF-8")
      (set-default-coding-systems 'utf-8)

      ;; let modeline and border in terminal to not be inverse video
      ;; (set-face-inverse-video 'mode-line nil)
      (set-face-attribute 'mode-line nil :background "unspecified-bg")
      (set-face-attribute 'mode-line-inactive nil :background "unspecified-bg")
      (set-face-attribute 'vertical-border nil :background "unspecified-bg")

      ;; show pressed keys in modeline
      (keycast-mode-line-mode)

      ;; swtich on auto completion globally
      (add-hook 'after-init-hook 'global-company-mode)

      ;; paredit post config
      (dolist
        (m
          '(emacs-lisp-mode-hook
            eval-expression-minibuffer-setup-hook
            ielm-mode-hook
            lisp-mode-hook
            lisp-interaction-mode-hook
            scheme-mode-hook
            ;geiser-repl-mode-hook
	))
        (add-hook m #'enable-paredit-mode))
      ;; (bind-keys :map paredit-mode-map
      ;;   ("{" . paredit-open-curly)
      ;;   ("}" . paredit-close-curly))
      ;; (unless terminal-frame
      ;;   (bind-keys :map paredit-mode-map
      ;;     ("M-[" . paredit-wrap-square)
      ;;     ("M-{" . paredit-wrap-curly)))

      ;; org mode hook for auto completion
      (add-hook 'org-mode-hook 'company-mode)

      (require 'company-org-block)
      (setq company-org-block-edit-style 'auto) ;; 'auto, 'prompt, or 'inline

      (add-hook 'org-mode-hook
        (lambda ()
          (add-to-list (make-local-variable 'company-backends)
            'company-org-block)))      ;; helm config

      (marginalia-mode)
      (global-set-key (kbd "M-x") #'helm-M-x)
      (global-set-key (kbd "C-x b") #'helm-buffers-list)
      (global-set-key (kbd "C-c h") #'helm-command-prefix)
      (global-set-key (kbd "C-c f") #'helm-recentf)
      (global-set-key (kbd "C-c b") #'helm-bookmarks)      
      (global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
      (global-set-key (kbd "C-x C-f") #'helm-find-files)
      (eval-after-load 'company
        '(progn
           (define-key company-mode-map (kbd "C-:") 'helm-company)
           (define-key company-active-map (kbd "C-:") 'helm-company)))
      (helm-mode 1)
      (setf helm-buffers-fuzzy-matching t)
      ;; (bind-keys :map helm-command-map
      ;;   ("C-c h"   . helm-command-prefix)
      ;;   ("M-x"     . helm-M-x)
      ;;   ("C-x C-f" . helm-find-files)
      ;;   ("C-x b"   . helm-buffers-list)
      ;;   ("C-c b"   . helm-bookmarks)
      ;;   ("C-c f"   . helm-recentf)
      ;;   ("C-c g"   . helm-grep-do-git-grep))

      ;; Python language support related stuff. Adapted from https://gitlab.com/nathanfurnal/dotemacs/-/snippets/2060535
      ;; ;; Provides workspaces with file browsing (tree file viewer)
      ;; ;; and project management when coupled with `projectile`.
      (use-package treemacs
        :ensure nil
        :defer t
        :config
        (setq treemacs-no-png-images t
	            treemacs-width 24)
        :bind ("C-c t" . treemacs))
      ;; ;; Provide LSP-mode for python, it requires a language server.
      ;; ;; I use `lsp-pyright`. Know that you have to `M-x lsp-restart-workspace` 
      ;; ;; if you change the virtual environment in an open python buffer.
      (use-package lsp-mode
        :ensure nil
        :defer t
        :commands (lsp lsp-deferred)
        :init (setq lsp-keymap-prefix "C-c l")
        :hook (python-mode . lsp-deferred))
      ;; ;; Provides completion, with the proper backend
      ;; ;; it will provide Python completion.
      (use-package company
        :ensure nil
        :defer t
        :diminish
        :config
        (setq company-dabbrev-other-buffers t
         company-dabbrev-code-other-buffers t)
        :hook ((text-mode . company-mode)
               (prog-mode . company-mode)))
      ;; ;; Provides visual help in the buffer 
      ;; ;; For example definitions on hover. 
      ;; ;; The `imenu` lets me browse definitions quickly.
      (use-package lsp-ui
        :ensure nil
        :defer t
        :config
        (setq lsp-ui-sideline-enable nil
	       lsp-ui-doc-delay 2)
        :hook (lsp-mode . lsp-ui-mode)
        :bind (:map lsp-ui-mode-map
	                  ("C-c i" . lsp-ui-imenu)))
      ;; ;; Integration with the debug server 
      ;; (use-package dap-mode
      ;;   :ensure nil
      ;;   :defer t
      ;;   :after lsp-mode
      ;;   :config
      ;;   (dap-auto-configure-mode))
      ;; ;; Built-in Python utilities
      (use-package python
        :ensure nil
        :config
        ;; Remove guess indent python message
        (setq python-indent-guess-indent-offset-verbose nil)
        ;; Use IPython when available or fall back to regular Python 
        (cond
         ((executable-find "ipython")
          (progn
            (setq python-shell-buffer-name "IPython")
            (setq python-shell-interpreter "ipython")
            (setq python-shell-interpreter-args "-i --simple-prompt")))
         ((executable-find "python3")
          (setq python-shell-interpreter "python3"))
         ((executable-find "python2")
          (setq python-shell-interpreter "python2"))
         (t
          (setq python-shell-interpreter "python"))))
      ;; ;; Language server for Python 
      ;; ;; Read the docs for the different variables set in the config.
      (use-package lsp-pyright
        :ensure nil
        :defer t
        :config
        ;; (setq lsp-clients-python-library-directories '("/usr/" "~/miniconda3/pkgs"))
        (setq lsp-pyright-disable-language-service nil
	            lsp-pyright-disable-organize-imports nil
	            lsp-pyright-auto-import-completions t
	            lsp-pyright-use-library-code-for-types t
	            ;; lsp-pyright-venv-path "~/miniconda3/envs"
        )
        :hook ((python-mode . (lambda () 
                                (require 'lsp-pyright) (lsp-deferred)))))
      ;; ;; Format the python buffer following YAPF rules
      ;; ;; There's also blacken if you like it better.
      (use-package yapfify
        :ensure nil
        :defer t
        :hook (python-mode . yapf-mode))

      ;; Better unicode support
      (use-package unicode-fonts
        :ensure nil
        :config
        (unicode-fonts-setup))

      ;; orgmode language activation
      (org-babel-do-load-languages
        'org-babel-load-languages
        '((emacs-lisp   . t)
          (shell   . t)
          (haskell . t)))
      ;; orgmode open source window by vertically splitting on right
      (setf org-src-window-setup 'split-window-right)

      ;; maintain a list of most recent files opened
      (recentf-mode 1)
      (setf recentf-max-saved-items 32)

      ;; enable projectile
      (projectile-mode +1)

      ;; enable which-key
      (which-key-mode)

      ;; turn on yassnippets globally
      (yas-global-mode)

      ;; turn on flycheck globally
      (global-flycheck-mode)

      ;; support for haskell
      (require 'lsp-mode)
      (add-hook 'haskell-mode-hook #'lsp)
      (add-hook 'haskell-literate-mode-hook #'lsp)

      ;; support for scala + metals + eglot.
      (add-hook 'scala-mode-hook 'eglot-ensure)

      ;; turn on lsp-java for java files
      (add-hook 'java-mode-hook 'lsp)

      ;; support for c and c++
      (add-hook 'c-mode-hook 'lsp)
      (add-hook 'c++-mode-hook 'lsp)

      ;; support for spring boot
      ;(require 'lsp-java-boot)
      ;(add-hook 'lsp-mode-hook #'lsp-lens-mode)
      ;(add-hook 'java-mode-hook #'lsp-java-boot-lens-mode)

      ;; support for xml folding
      ;(require 'hideshow)
      ;(require 'sgml-mode)
      ;(require 'nxml-mode)
      ;(add-to-list 'hs-special-mode-alist
      ;  '(nxml-mode
      ;    "<!--\\|<[^/>]*[^/]>"
      ;    "-->\\|</[^/>]*[^/]>"

      ;    "<!--"
      ;    sgml-skip-tag-forward
      ;    nil))
      ;(add-hook 'nxml-mode-hook 'hs-minor-mode)
      ;(define-key 'nxml-mode-map (kbd "C-c h") 'hs-toggle-hiding)

      ;; set custom theme
      (when (display-graphic-p)
        (add-hook 'after-init-hook (lambda () (load-theme 'sanityinc-solarized-light))))

      ;; set fonts
      ;; (custom-set-faces
      ;;  '(default   ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight bold :height 200 :width normal))))
      ;;  ;'(mode-line ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight bold :height 143 :width normal))))
      ;; )

      ;; Support for Rust language
      (require 'rust-mode)
      ;; ;; Indentation
      (add-hook 'rust-mode-hook
          (lambda () (setq indent-tabs-mode nil)))
      ;; ;; Code formatting
      (setq rust-format-on-save t)
      ;; ;; Prettifying
      (add-hook 'rust-mode-hook
          (lambda () (prettify-symbols-mode)))
      ;; ;; LSP mode
      (add-hook 'rust-mode-hook #'lsp)

      (custom-set-faces
       ;; custom-set-faces was added by Custom.
       ;; If you edit it by hand, you could mess it up, so be careful.
       ;; Your init file should contain only one such instance.
       ;; If there is more than one, they won't work right.
       ;; '(default ((t (:family "FiraCode Nerd Font Mono" :foundry "CTDB" :slant normal :weight bold :height 150 :width normal))))
       '(default ((t (:family "Cascadia Code" :foundry "CTDB" :slant normal :weight bold :height 150 :width normal)))))
  
      ;; disable splash screen
      (setf inhibit-splash-screen t)
      (setf inhibit-startup-screen t)
      (setf inhibit-startup-message t)

      ;; highlight closing parens
      (show-paren-mode 1)

      ;; modebar metrics
      (column-number-mode t)
      (setf display-time-date-and-day t)

      ;; cleanup the ui
      (menu-bar-mode -1)
      (scroll-bar-mode -1)
      (tool-bar-mode -1)
      (tooltip-mode -1)
      (fringe-mode 0)
      (setf inhibit-startup-message t)
    '';
  };

  # programs.java.enable = true;
  # programs.java.package = pkgs.openjdk8;
  # # programs.java.package = pkgs.openjdk11;
  # # programs.java.package = pkgs.openjdk19;

  programs.bash = {
    enable = true;
    profileExtra = ''
      # source "$HOME/.sdkman/bin/sdkman-init.sh"
      if [ -e $HOME/.nix-profile/etc/profile.d/nix.sh ]; then
        . $HOME/.nix-profile/etc/profile.d/nix.sh
      fi
    '';
  };

  # You can also manage environment variables but you will have to manually
  # source
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/prateem/etc/profile.d/hm-session-vars.sh
  #
  # if you don't want to manage your shell through Home Manager.
  home.sessionVariables = {
    # EDITOR = "emacs";
    # JAVA_HOME="$HOME/.nix-profile/lib/openjdk";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
